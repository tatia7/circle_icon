package com.example.circle_icon

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.annotation.NonNull
import com.ramotion.circlemenu.CircleMenuView

class MainActivity : AppCompatActivity() {
    private lateinit var visibilityButton : ImageButton
    private lateinit var icon_1 : ImageButton
    private lateinit var icon_2 : ImageButton
    private lateinit var icon_3 : ImageButton
    private lateinit var icon_4 : ImageButton
    private lateinit var icon_5 : ImageButton
    private lateinit var icon_6 : ImageButton
    private lateinit var icon_7 : ImageButton
    private lateinit var icon_8 : ImageButton
    private lateinit var text : TextView
    private lateinit var hellokitty : TextView
    private lateinit var kuromi : TextView
    private lateinit var keroppi : TextView
    private lateinit var chococat : TextView
    private lateinit var purin : TextView
    private lateinit var pippo : TextView
    private lateinit var mymelody : TextView
    private lateinit var badtemaru : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        visibilityButton.setOnClickListener {
            visibilityChange()
        }

    }
    private fun init(){
        visibilityButton = findViewById(R.id.visibility_button)
        text = findViewById(R.id.text)
        hellokitty = findViewById(R.id.helloKitty)
        kuromi = findViewById(R.id.kuromi)
        keroppi = findViewById(R.id.keroppi)
        chococat = findViewById(R.id.chococat)
        purin = findViewById(R.id.purin)
        pippo = findViewById(R.id.pippo)
        mymelody = findViewById(R.id.mymelody)
        badtemaru = findViewById(R.id.badtemaru)
        icon_1 = findViewById(R.id.icon_1)
        icon_2 = findViewById(R.id.icon_2)
        icon_3 = findViewById(R.id.icon_3)
        icon_4 = findViewById(R.id.icon_4)
        icon_5 = findViewById(R.id.icon_5)
        icon_6 = findViewById(R.id.icon_6)
        icon_7 = findViewById(R.id.icon_7)
        icon_8 = findViewById(R.id.icon_8)


    }
    private fun visibilityChange(){
        if (icon_1.visibility == View.VISIBLE){
            text.setText("Click to see!")
            hellokitty.visibility = View.INVISIBLE
            kuromi.visibility = View.INVISIBLE
            keroppi.visibility = View.INVISIBLE
            chococat.visibility = View.INVISIBLE
            purin.visibility = View.INVISIBLE
            pippo.visibility = View.INVISIBLE
            mymelody.visibility = View.INVISIBLE
            badtemaru.visibility = View.INVISIBLE
            icon_1.visibility = View.INVISIBLE
            icon_2.visibility = View.INVISIBLE
            icon_3.visibility = View.INVISIBLE
            icon_4.visibility = View.INVISIBLE
            icon_5.visibility = View.INVISIBLE
            icon_6.visibility = View.INVISIBLE
            icon_7.visibility = View.INVISIBLE
            icon_8.visibility = View.INVISIBLE
        }else{
            text.setText("Make them disappear!")
            hellokitty.visibility = View.VISIBLE
            kuromi.visibility = View.VISIBLE
            keroppi.visibility = View.VISIBLE
            chococat.visibility = View.VISIBLE
            purin.visibility = View.VISIBLE
            pippo.visibility = View.VISIBLE
            mymelody.visibility = View.VISIBLE
            badtemaru.visibility = View.VISIBLE
            icon_1.visibility = View.VISIBLE
            icon_2.visibility = View.VISIBLE
            icon_3.visibility = View.VISIBLE
            icon_4.visibility = View.VISIBLE
            icon_5.visibility = View.VISIBLE
            icon_6.visibility = View.VISIBLE
            icon_7.visibility = View.VISIBLE
            icon_8.visibility = View.VISIBLE
        }



    }
}